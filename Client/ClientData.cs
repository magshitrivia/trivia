﻿using System.Net.Sockets;
using System.Text;

namespace Client
{
    public static class ClientData
    {
        public static TcpClient client;
        public static NetworkStream netStream;
        public static CryptoDevice device; 
        public static string username;

        public static void Send(string msg)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(msg);
            netStream.Write(buffer, 0, buffer.Length);
        }

        public static string Recieve(int n)
        {
            byte[] buffer = new byte[n];
            int read = netStream.Read(buffer, 0, buffer.Length);
            return Encoding.ASCII.GetString(buffer, 0, read);
        }
    }
}