﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Client
{
    public partial class CreateRoom : Form
    {
        public CreateRoom()
        {
            InitializeComponent();
        }

        private void btnCreateRoom_Click(object sender, EventArgs e)
        {
            int.TryParse(txtPlayers.Text, out int numOfPlayers);
            int.TryParse(txtQuestions.Text, out int numOfQuestions);
            int.TryParse(txtTime.Text, out int time);

            if (Enumerable.Range(1, 9).Contains(numOfPlayers) && Enumerable.Range(1, 99).Contains(numOfQuestions) && Enumerable.Range(1, 99).Contains(time))
            {
                ClientData.Send($"0213{txtName.Text.Length.ToString().PadLeft(2, '0')}{txtName.Text}{numOfPlayers}{numOfQuestions.ToString().PadLeft(2, '0')}{time.ToString().PadLeft(2, '0')}");

                if (ClientData.Recieve(4).Equals("1140"))
                {
                    Hide();
                    new Lobby(txtName.Text, true, numOfPlayers, numOfQuestions, time).ShowDialog();
                    Show();
                }
                else
                    MessageBox.Show("Can't create room.");
            }
            else
                MessageBox.Show("Invaild parameters.");
            
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}