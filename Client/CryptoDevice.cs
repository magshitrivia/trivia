﻿using System.Text;

namespace Client
{
    public class CryptoDevice
    {
        private uint p, q, phi, d;

        public CryptoDevice()
        {
            p = 37;
            q = 31;

            N = p * q;
            phi = (p - 1) * (q - 1);
            E = 7;

            d = MulInv(E, phi);
        }

        public string Encrypt(string plain)
        {
            uint[] plainBytes = new uint[plain.Length];

            for (int i = 0; i < plain.Length; i++)
                plainBytes[i] = plain[i];

            for (int i = 0; i < plainBytes.Length; i++)
                plainBytes[i] = PowMod(plainBytes[i], OtherE, OtherN);

            StringBuilder builder = new StringBuilder();

            foreach (uint i in plainBytes) builder.Append($"{i},");

            builder.Length--;

            return builder.ToString();
        }

        public string Decrypt(string cipher)
        {
            uint[] cipherBytes;

            string[] splits = cipher.Split(',');

            cipherBytes = new uint[splits.Length];

            for (int i = 0; i < splits.Length; i++)
                cipherBytes[i] = uint.Parse(splits[i]);

            for (int i = 0; i < cipherBytes.Length; i++)
                cipherBytes[i] = PowMod(cipherBytes[i], d, N);

            StringBuilder builder = new StringBuilder();

            foreach (uint i in cipherBytes) builder.Append((char)i);

            return builder.ToString();
        }

        public uint E { get; set; }
        public uint N { get; set; }

        public uint OtherE { get; set; }
        public uint OtherN { get; set; }

        private uint PowMod(uint a, uint b, uint n)
        {
            uint result = 1;

            for (int i = 1; i <= b; i++)
                result = ((result * a) % n);

            return result;
        }

        private uint MulInv(uint a, uint b)
        {
            uint b0 = b, t, q;
            uint x0 = 0, x1 = 1;

            if (b == 1) return 1;

            while (a > 1)
            {
                q = a / b;
                t = b;
                b = a % b;
                a = t;
                t = x0;
                x0 = x1 - q * x0;
                x1 = t;
            }

            if (x1 < 0) x1 += b0;

            return x1;
        }
    }
}