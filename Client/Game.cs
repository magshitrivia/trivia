﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using static Client.ClientData;
using System.Media;
using System.IO;

namespace Client
{
    public partial class Game : Form
    {
        private int timeLeft;
        private int time;
        private int ans;
        private Button button;
        private SoundPlayer player;

        bool finish = false;

        Thread t;

        public Game(int numQuestions, int time)
        {
            InitializeComponent();
            player = new SoundPlayer();
            player.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + "kahoot.wav";
            player.PlayLooping();

            this.time = time;

             t = new Thread(new ThreadStart(() =>
             {
                while (!finish)
                {
                    int code = int.Parse(Recieve(3));

                    if (code == 120)
                    {
                        Invoke((MethodInvoker)delegate { button.BackColor = Recieve(1).Equals("1") ? Color.Green : Color.Red; });
                        Thread.Sleep(1000);
                        //Invoke.
                        button.BackColor = SystemColors.Control;
                    }
                    else if (code == 118)
                        Invoke((MethodInvoker)delegate { NextQuestion(); });
                    else if(code == 121)
                    {
                        finish = true;
                        player.Stop();

                        int numPlayers = int.Parse(Recieve(1));

                        string scores = "", user;
                        for (int i = 0; i < numPlayers; i++)
                        {
                            int len = int.Parse(Recieve(2));
                            user = Recieve(len);
                            scores += $"User: {user}, Score: {Recieve(2)}\n";
                        }

                        Invoke((MethodInvoker)delegate { MessageBox.Show(scores); Hide(); new Menu().ShowDialog(); });   
                    }
                    else if (code == 150)
                    {
                        int usernameLen = int.Parse(Recieve(2));
                        string username = Recieve(usernameLen);

                        int msgLen = int.Parse(Recieve(2));
                        string message = Recieve(msgLen);

                        Invoke((MethodInvoker)delegate { chatBox.Items.Add($"{username}: {message}"); });
                    }
                }
            }));
            
            // Handle first question.
            NextQuestion();

            t.Start();
        }

        private void NextQuestion()
        {
            string question;
            int questionLen = int.Parse(Recieve(3));
            question = Recieve(questionLen);

            string[] answers = new string[4];

            for (int i = 0; i < 4; i++)
            {
                int ansLen = int.Parse(Recieve(3));
                string ans = Recieve(ansLen);
                answers[i] = ans;
            }

            lblQuestion.Text = question;
            btnAns1.Text = answers[0];
            btnAns2.Text = answers[1];
            btnAns3.Text = answers[2];
            btnAns4.Text = answers[3];

            timeLeft = time;
            timeLabel.Text = $"{time} seconds";
            ans = 0;

            timer1.Start();
        }

        private void ans_Click(object sender, EventArgs e)
        {
            button = (Button)sender;
            ans = button.Name[button.Name.Length - 1] - '0';
        }

        private void btnLeave_Click(object sender, EventArgs e)
        {
            Send("0222");
            player.Stop();
            finish = true;
            timer1.Stop();

            t.Interrupt();
            t.Abort();

            DialogResult = DialogResult.OK;

            //Hide();
            //new Menu().ShowDialog();
            //Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ans != 0) //user answered
            {
                timer1.Stop();
                Send($"0219{ans}{(time - timeLeft).ToString().PadLeft(2, '0')}");
            }
            else if (timeLeft > 0)
            {
                timeLeft--;
                timeLabel.Text = $"{timeLeft} seconds";
            }
            else //user ran out of time
            {
                timeLabel.Text = "Time's up!";
                timer1.Stop();
                Send($"0219{5}{(time - timeLeft).ToString().PadLeft(2, '0')}");
            }
        }

        private void txtMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                string message = txtMessage.Text;
                Send($"0250{message.Length.ToString().PadLeft(2, '0')}{message}");
                txtMessage.Text = "";
            }
        }
    }
}