﻿using System;
using System.Windows.Forms;

using static Client.ClientData;

namespace Client
{
    public partial class Join : Form
    {
        public Join()
        {
            InitializeComponent();
        }

        private void Join_Load(object sender, EventArgs e)
        {
            lbUsers.Hide();
            LoadRooms();
        }

        private void lbRooms_SelectedIndexChanged(object sender, EventArgs e)
        {
            string curItem = null;
            if (lbRooms.SelectedItem != null)
                curItem = lbRooms.SelectedItem.ToString();

            Send($"0207{curItem.Split(':')[0].PadLeft(4, '0')}");

            lbUsers.Items.Clear();
            lbUsers.Show();

            string msgType = Recieve(3), username;
            int numOfUsers = int.Parse(Recieve(1)), nameLen;
            for (int i = 0; i < numOfUsers; i++)
            {
                nameLen = int.Parse(Recieve(2));
                username = Recieve(nameLen);

                lbUsers.Items.Add(username);
            }
        }
   
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadRooms();
        }

        private void btnJoin_Click(object sender, EventArgs e)
        {
            Send($"0209{lbRooms.SelectedItem.ToString().Split(':')[0].PadLeft(4, '0')}");
            int code = int.Parse(Recieve(4));

            if (code == 1100)
            {
                int questionNum = int.Parse(Recieve(2));
                int time = int.Parse(Recieve(2));

                Hide();
                new Lobby(lbRooms.SelectedItem.ToString(), false, 0, questionNum, time).ShowDialog();
                Show();
            }
            else if (code == 1101)
                MessageBox.Show("room is full");
            else if(code == 1102)
                MessageBox.Show("room not exist or other reason");
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void LoadRooms()
        {
            Send("0205");
            lbRooms.Items.Clear();
            string msgType = Recieve(3), roomName;
            int numOfRooms = int.Parse(Recieve(4)), roomID, nameLen;
            for(int i = 0; i < numOfRooms;i++)
            {
                roomID = int.Parse(Recieve(4));
                nameLen = int.Parse(Recieve(2));
                roomName = Recieve(nameLen);

                lbRooms.Items.Add($"{roomID}:{roomName}");
            }
        }
    }  
}