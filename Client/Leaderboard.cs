﻿using System;
using System.Data;
using System.Windows.Forms;

using static Client.ClientData;

namespace Client
{
    public partial class Leaderboard : Form
    {
        public Leaderboard()
        {
            InitializeComponent();
        }

        private void Leaderboard_Load(object sender, EventArgs e)
        {
            Send("0223");

            int code = int.Parse(Recieve(3));
            if(code == 124)
            {
                DataTable dt = new DataTable();

                dt.Columns.Add("Name");
                dt.Columns.Add("Score");

                for (int i = 0; i < 3; i++)
                {
                    int usernameLen = int.Parse(Recieve(2));
                    string username = usernameLen > 0 ? Recieve(usernameLen) : "0";

                    int score = int.Parse(Recieve(6));

                    dt.Rows.Add(username, score);
                }

                dataGridView1.DataSource = dt;
                dataGridView1.RowHeadersVisible = false;
                dataGridView1.AllowUserToResizeColumns = false;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridView1.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}