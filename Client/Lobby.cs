﻿using System;
using System.Threading;
using System.Windows.Forms;

using static Client.ClientData;

namespace Client
{
    public partial class Lobby : Form
    {
        private string roomName;
        private int roomId;
        private int numQuestions;
        private int time;

        private Thread t;

        public Lobby(string roomName, bool admin, int numPlayers, int numQuestions, int time)
        {
            InitializeComponent();
            lblDetails.Text = "";

            this.numQuestions = numQuestions;
            this.time = time;

            if (admin)
            {
                lblDetails.Text = $"Max players {numPlayers}";
                btnClose.Visible = true;
                btnStart.Visible = true;
                this.roomName = roomName;
                roomId = RoomId();
            }
            else
            {
                btnLeave.Visible = true;
                this.roomName = roomName.Split(':')[1];
                roomId = int.Parse(roomName.Split(':')[0]);
            }

            lblDetails.Text += $", Number of Questions: {numQuestions}   Time per question: {time}";

            lblConnected.Text = $"You are connected to room: {roomName}";

            LoadUsers();
        }

        private int RoomId()
        {
            Send("0205");
            int code = int.Parse(Recieve(3));

            if (code == 106)
            {
                int numOfRooms = int.Parse(Recieve(4));
                if (numOfRooms > 0)
                {
                    for (int i = 0; i < numOfRooms; i++)
                    {
                        int roomId = int.Parse(Recieve(4));
                        int roomNameLen = int.Parse(Recieve(2));
                        string roomName = Recieve(roomNameLen);

                        if (roomName.Equals(this.roomName))
                            return roomId;
                    }
                }
            }

            return -1;
        }

        private void LoadUsers()
        {
            lbUsers.Items.Clear();
            lbUsers.Show();

            Send($"0207{roomId.ToString().PadLeft(4, '0')}");

            int code = int.Parse(Recieve(3));
            if (code == 108)
            {
                int numOfUsers = int.Parse(Recieve(1));

                for (int i = 0; i < numOfUsers; i++)
                {
                    int nameLen = int.Parse(Recieve(2));
                    string username = Recieve(nameLen);

                    lbUsers.Items.Add(username);
                }
            }
        }

        private void btnLeave_Click(object sender, EventArgs e)
        {
            Send("0211");

            t.Interrupt();
            t.Abort();

            DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Send("0215");

            t.Interrupt();
            t.Abort();

            DialogResult = DialogResult.OK;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Send("0217");
        }

        private void Lobby_Load(object sender, EventArgs e)
        {
            t = new Thread(new ThreadStart(() =>
            {
                bool stop = false;

                while (!stop)
                {
                    int code = int.Parse(Recieve(3));

                    if (code == 108)
                    {
                        Invoke((MethodInvoker)delegate
                        {
                           lbUsers.Items.Clear();
                           lbUsers.Show();

                           int numOfUsers = int.Parse(Recieve(1));

                           for (int i = 0; i < numOfUsers; i++)
                           {
                               int nameLen = int.Parse(Recieve(2));
                               string username = Recieve(nameLen);

                               lbUsers.Items.Add(username);
                           }
                       });
                    }
                    else if (code == 118)
                    {
                        stop = true;

                        Invoke((MethodInvoker)delegate
                        {
                            Hide();
                            new Game(numQuestions, time).ShowDialog();
                            DialogResult = DialogResult.OK;
                        });
                    }
                    else if (code == 150)
                    {
                        int usernameLen = int.Parse(Recieve(2));
                        string username = Recieve(usernameLen);

                        int msgLen = int.Parse(Recieve(2));
                        string message = Recieve(msgLen);

                        Invoke((MethodInvoker)delegate { chatBox.Items.Add($"{username}: {message}"); });
                    }
                }
            }));

            t.Start();
        }

        private void txtMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                string message = txtMessage.Text;
                Send($"0250{message.Length.ToString().PadLeft(2, '0')}{message}");
                txtMessage.Text = "";
            }
        }
    }
}