﻿using System;
using System.Windows.Forms;

namespace Client
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
            lblHello.Text = $"Hello {ClientData.username}";
        }

        private void btnSignout_Click(object sender, EventArgs e)
        {
            ClientData.Send("0201");
            DialogResult = DialogResult.OK;
        }

        private void btnStats_Click(object sender, EventArgs e)
        {
            Hide();
            new Stats().ShowDialog();
            Show();
        }

        private void btnScores_Click(object sender, EventArgs e)
        {
            Hide();
            new Leaderboard().ShowDialog();
            Show();
        }

        private void btnCreateRoom_Click(object sender, EventArgs e)
        {
            Hide();
            new CreateRoom().ShowDialog();
            Close();
        }

        private void btnJoinRoom_Click(object sender, EventArgs e)
        {
            Hide();
            new Join().ShowDialog();
            Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();  
            //DialogResult = DialogResult.OK;
        }
    }
}