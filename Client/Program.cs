﻿using System;
using System.Windows.Forms;
using System.Net.Sockets;

using static Client.ClientData;

namespace Client
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                TcpClient client = new TcpClient("127.0.0.1", 8820);
                NetworkStream ns = client.GetStream();
                CryptoDevice device = new CryptoDevice();

                ClientData.client = client;
                ClientData.device = device;
                netStream = ns;

                // Here we will exchange keys with the server.
                Send($"{device.E},{device.N}");

                string[] splits = Recieve(1024).Split(',');

                device.OtherE = uint.Parse(splits[0]);
                device.OtherN = uint.Parse(splits[1]);
                
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new WelcomeForm());

                Send("0299");
            }
            catch
            {
                MessageBox.Show("You don't have server running!");
            }
        }
    }
}