﻿using System;
using System.Net;
using System.Net.Mail;
using System.Windows.Forms;

using static Client.ClientData;

namespace Client
{
    public partial class ResetPassword : Form
    {
        private int verfiyCode;
        private string email;

        public ResetPassword()
        {
            InitializeComponent();

            Random rnd = new Random();
            verfiyCode = rnd.Next(100000, 1000000);
        }

        private void SendMail(string to)
        {
            MailMessage mail = new MailMessage("yarivwork1@gmail.com", to);
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("yarivwork1", "Yariv123");
            client.Host = "smtp.gmail.com";
            mail.Subject = "Reset Password.";
            mail.Body = $"Reset Code: {verfiyCode}";
            client.Send(mail);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(label1.Text == "Enter email:")
            {
                string email = textBox1.Text;

                Send($"0230{email.Length.ToString().PadLeft(3, '0')}{email}");

                this.email = email;

                int code = int.Parse(Recieve(3));

                if (code == 130)
                {
                    SendMail(email);
                    label1.Text = "Enter verfication code:";
                    button1.Text = "Verfiy.";
                    textBox1.Text = "";
                }
                else if (code == 131)
                    MessageBox.Show("Invaild email.");
            }
            else if(label1.Text == "Enter verfication code:")
            {
                if (int.Parse(textBox1.Text) == verfiyCode)
                {
                    label1.Text = "Enter your new password:";
                    button1.Text = "Reset.";
                    textBox1.Text = "";
                }
                else
                    MessageBox.Show("Wrong verfication code.");
            }
            else if(label1.Text == "Enter your new password:")
            {
                if (textBox1.Text != "")
                {
                    Send($"0240{textBox1.Text.Length.ToString().PadLeft(2, '0')}{textBox1.Text}{email.Length.ToString().PadLeft(3, '0')}{email}");

                    if (int.Parse(Recieve(3)) == 140) DialogResult = DialogResult.OK;
                }
                else
                    MessageBox.Show("You must enter password.");
            }
        }
    }
}