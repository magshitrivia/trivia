﻿using System;
using System.Windows.Forms;

using static Client.ClientData;

namespace Client
{
    public partial class Signup : Form
    {
        public Signup()
        {
            InitializeComponent();
        }

        private void btnSignup_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text.Equals(rePassword.Text))
            {
                Send("1");
                string cipher = device.Encrypt($"203{txtUsername.Text.Length.ToString().PadLeft(2, '0')}{txtUsername.Text}{txtPassword.Text.Length.ToString().PadLeft(2, '0')}{txtPassword.Text}{txtEmail.Text.Length.ToString().PadLeft(2, '0')}{txtEmail.Text}");
                Send(cipher);

                if (Check())
                {
                    Send("1"); // When you send 1 before the message itself it means that the message you about to send is encrypted, 0 stands for not encrypted.
                    cipher = device.Encrypt($"200{txtUsername.Text.Length.ToString().PadLeft(2, '0')}{txtUsername.Text}{txtPassword.Text.Length.ToString().PadLeft(2, '0')}{txtPassword.Text}");
                    Send(cipher);

                    if (!Recieve(4).Equals("1020"))
                        MessageBox.Show("Some error occurred.");
                    else
                    {
                        Hide();
                        new Menu().ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("Passwords don't match!");
        }

        private bool Check()
        {
            string serverResponse = Recieve(4);

            if (serverResponse.Equals("1040"))
                return true;
            else if (serverResponse.Equals("1041"))
                lblResponse.Text = "Illegal password";
            else if (serverResponse.Equals("1042"))
                lblResponse.Text = "Username is already exists";
            else if (serverResponse.Equals("1043"))
                lblResponse.Text = "Username is illegal";
            else if (serverResponse.Equals("1044"))
                lblResponse.Text = "Other";

            return false;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}