﻿namespace Client
{
    partial class Stats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUser = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblNumOfGames = new System.Windows.Forms.Label();
            this.lblNumOfCorrect = new System.Windows.Forms.Label();
            this.lblNumOfWrong = new System.Windows.Forms.Label();
            this.lblAvgTime = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(13, 60);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(39, 13);
            this.lblUser.TabIndex = 0;
            this.lblUser.Text = "<user>";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(128, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "My Performances:";
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(197, 12);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblNumOfGames
            // 
            this.lblNumOfGames.AutoSize = true;
            this.lblNumOfGames.Location = new System.Drawing.Point(16, 95);
            this.lblNumOfGames.Name = "lblNumOfGames";
            this.lblNumOfGames.Size = new System.Drawing.Size(13, 13);
            this.lblNumOfGames.TabIndex = 4;
            this.lblNumOfGames.Text = "1";
            // 
            // lblNumOfCorrect
            // 
            this.lblNumOfCorrect.AutoSize = true;
            this.lblNumOfCorrect.Location = new System.Drawing.Point(16, 122);
            this.lblNumOfCorrect.Name = "lblNumOfCorrect";
            this.lblNumOfCorrect.Size = new System.Drawing.Size(13, 13);
            this.lblNumOfCorrect.TabIndex = 5;
            this.lblNumOfCorrect.Text = "2";
            // 
            // lblNumOfWrong
            // 
            this.lblNumOfWrong.AutoSize = true;
            this.lblNumOfWrong.Location = new System.Drawing.Point(16, 147);
            this.lblNumOfWrong.Name = "lblNumOfWrong";
            this.lblNumOfWrong.Size = new System.Drawing.Size(13, 13);
            this.lblNumOfWrong.TabIndex = 6;
            this.lblNumOfWrong.Text = "3";
            // 
            // lblAvgTime
            // 
            this.lblAvgTime.AutoSize = true;
            this.lblAvgTime.Location = new System.Drawing.Point(17, 174);
            this.lblAvgTime.Name = "lblAvgTime";
            this.lblAvgTime.Size = new System.Drawing.Size(13, 13);
            this.lblAvgTime.TabIndex = 7;
            this.lblAvgTime.Text = "4";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Client.Properties.Resources.trivia_challenge;
            this.pictureBox1.InitialImage = global::Client.Properties.Resources.trivia_challenge;
            this.pictureBox1.Location = new System.Drawing.Point(82, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // Stats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblAvgTime);
            this.Controls.Add(this.lblNumOfWrong);
            this.Controls.Add(this.lblNumOfCorrect);
            this.Controls.Add(this.lblNumOfGames);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblUser);
            this.Name = "Stats";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Stats";
            this.Load += new System.EventHandler(this.Stats_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblNumOfGames;
        private System.Windows.Forms.Label lblNumOfCorrect;
        private System.Windows.Forms.Label lblNumOfWrong;
        private System.Windows.Forms.Label lblAvgTime;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}