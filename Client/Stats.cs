﻿using System;
using System.Windows.Forms;

using static Client.ClientData;

namespace Client
{
    public partial class Stats : Form
    {
        public Stats()
        {
            InitializeComponent();
            lblUser.Text = $"{username}'s Stats:";
        }

        private void Stats_Load(object sender, EventArgs e)
        {
            Send("0225");
            LoadStats();
        }

        private void LoadStats()
        {
            string serverResponse = Recieve(23);

            if (serverResponse.Equals("1260000"))
            {
                lblNumOfGames.Text = "Number of games played: 0";
                lblNumOfCorrect.Text = "Number of correct answers: 0";
                lblNumOfWrong.Text = "Number of incorrect answers: 0";
                lblAvgTime.Text = "Avarage response time: 0";
            }
            else
            {
                lblNumOfGames.Text = "Number of games played: " + serverResponse.Substring(3, 4).TrimStart('0');
                lblNumOfCorrect.Text = "Number of correct answers: " + serverResponse.Substring(7, 6).TrimStart('0');
                lblNumOfWrong.Text = "Number of incorrect answers: " + serverResponse.Substring(13, 6).TrimStart('0');
                lblAvgTime.Text = $"Avarage response time: {serverResponse.Substring(19, 2).TrimStart('0')}.{serverResponse.Substring(21, 2)}";
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}