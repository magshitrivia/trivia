﻿using System;
using System.Windows.Forms;

using static Client.ClientData;

namespace Client
{
    public partial class WelcomeForm : Form
    {
        public WelcomeForm()
        {
            InitializeComponent();
        }
    
        private void btnLogin_Click(object sender, EventArgs e)
        {
            Send("1"); // When you send 1 before the message itself it means that the message you about to send is encrypted, 0 stands for not encrypted.
            string cipher = device.Encrypt($"200{txtUsername.Text.Length.ToString().PadLeft(2, '0')}{txtUsername.Text}{txtPassword.Text.Length.ToString().PadLeft(2, '0')}{txtPassword.Text}");
            Send(cipher);
            
            if (Check())
            {
                username = txtUsername.Text;

                Hide();
                new Menu().ShowDialog();
                //Show();
                Close();
            }
        }

        private bool Check()
        {
            string serverResponse = Recieve(4);

            if (serverResponse.Equals("1020"))
                return true;
            else if (serverResponse.Equals("1021"))
                lblVal.Text = "Wrong Details";
            else if (serverResponse.Equals("1022"))
                lblVal.Text = "User is already connected";

            return false;
        }

        private void btnSignup_Click(object sender, EventArgs e)
        {
            Hide();
            new Signup().ShowDialog();
            //Show();
            Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Hide();
            new ResetPassword().ShowDialog();
            //Show();
            Close();
        }
    }
}