#include "CryptoDevice.h"

CryptoDevice::CryptoDevice()
{
	p = 23;
	q = 31;

	N = p * q;

	int phi = (p - 1) * (q - 1);
	e = 17;

	d = mulInv(e, phi);
}

CryptoDevice::~CryptoDevice() {}

//string CryptoDevice::md5(const string& text)
//{
//	byte digest[CryptoPP::Weak::MD5::DIGESTSIZE];
//
//	CryptoPP::Weak::MD5 hash;
//	hash.CalculateDigest(digest, (const byte*)text.c_str(), text.length());
//
//	CryptoPP::HexEncoder encoder;
//	std::string output;
//
//	encoder.Attach(new CryptoPP::StringSink(output));
//	encoder.Put(digest, sizeof(digest));
//	encoder.MessageEnd();
//	string output;
//	return output;
//}

string CryptoDevice::encrypt(const string& plain)
{
	vector<uint> plainBytes;

	for (char c : plain) plainBytes.push_back(c); //O(n)

	for (uint i = 0; i < plainBytes.size(); i++)
		plainBytes[i] = pow_mod(plainBytes[i], otherE, otherN); //O(n^2)

	stringstream ss;

	for (uint i : plainBytes) ss << i << ","; // O(n)

	string res = ss.str(); // O(1)
	res.pop_back(); // O(1)

	return res;
}

string CryptoDevice::decrypt(const string& cipher)
{
	vector<uint> cipherBytes;

	string tmp;
	for (char c : cipher)
	{
		if (c == ',')
		{
			cipherBytes.push_back(atoi(tmp.c_str()));
			tmp = "";
		}
		else
			tmp += c;
	}

	if (tmp != "")
		cipherBytes.push_back(atoi(tmp.c_str()));

	for (uint i = 0; i < cipherBytes.size(); i++)
		cipherBytes[i] = pow_mod(cipherBytes[i], d, N); //O(n^2)

	stringstream ss;

	for (uint i : cipherBytes) ss << (char)i;// << ","; // O(n)

	string res = ss.str(); // O(1)
						   //res.pop_back(); // O(1)

	return res;
}

string CryptoDevice::getPublicKey(void) const
{
	stringstream ss;

	ss << e << "," << N;

	return ss.str();
}

void CryptoDevice::setOtherPublicKey(const string& s)
{
	otherE = atoi(s.substr(0, s.find(",")).c_str());
	otherN = atoi(s.substr(s.find(",") + 1).c_str());
}

uint CryptoDevice::pow_mod(uint a, uint b, uint n)
{
	uint result = 1;

	for (uint i = 1; i <= b; i++)
		result = ((result * a) % n);

	return result;
}

uint CryptoDevice::mulInv(uint a, uint b)
{
	uint b0 = b, t, q;
	uint x0 = 0, x1 = 1;

	if (b == 1) return 1;

	while (a > 1)
	{
		q = a / b;
		t = b, b = a % b, a = t;
		t = x0, x0 = x1 - q * x0, x1 = t;
	}

	if (x1 < 0) x1 += b0;

	return x1;
}