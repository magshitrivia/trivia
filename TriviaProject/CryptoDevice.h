#pragma once

#include <string>
#include <random>
#include <time.h>
#include <sstream>
#include <vector>

using std::string;
using std::stringstream;
using std::vector;

typedef unsigned int uint;

class CryptoDevice
{
public:
	CryptoDevice();
	~CryptoDevice();

	string encrypt(const string& plain);
	string decrypt(const string& cipher);

	string getPublicKey(void) const;
	void setOtherPublicKey(const string& s);

private:
	uint pow_mod(uint a, uint b, uint n);
	bool isPrime(uint num);
	uint gcd(uint a, uint b);
	uint mulInv(uint a, uint b);

	uint p;
	uint q;
	uint N;
	uint e;
	uint d;

	uint otherE;
	uint otherN;
};