#include "DataBase.h"

DataBase::DataBase() 
{
	_currGameId = atoi(LiteQuery::Execute(DB_PATH, "select game_id from t_games order by game_id desc limit 1;")[0]["game_id"].c_str());
}

DataBase::~DataBase() {}

bool DataBase::isUserExists(const string& username)
{
	stringstream ss;

	ss << "select * from t_users where username='" << username << "';";

	Table t = LiteQuery::Execute(DB_PATH, ss.str());

	return t.getRows().size() > 0;
}

bool DataBase::addNewUser(const string& username, const string& password, const string& email)
{
	if (isUserExists(username))
		return false;

	stringstream ss;

	ss << "insert into t_users values('" << username << "','" << password << "','" << email << "');";

	try
	{
		LiteQuery::ExecuteDML(DB_PATH, ss.str());
		return true;
	}
	catch (...)
	{
		return false;
	}
}

bool DataBase::isUserAndPassMatch(const string& username, const string& password)
{
	stringstream ss;
	Table table;

	ss << "select * from t_users where username='" << username << "' and password='" << password << "';";

	try
	{
		table = LiteQuery::Execute(DB_PATH, ss.str());
	}
	catch (exception& ex)
	{
		string res = ex.what();
		return false;
	}

	return table.getRows().size() > 0;
}

vector<Question*> DataBase::initQuestions(int questionsNo)
{
	vector<Question*> questions;
	stringstream ss;

	ss << "select * from t_questions order by random() limit " << questionsNo << ";";

	Table table = LiteQuery::Execute(DB_PATH, ss.str());

	for (auto row : table.getRows())
	{
		Question* question = new Question(atoi(row["question_id"].c_str()), row["question"], row["correct_ans"], row["ans2"], row["ans3"], row["ans4"]);
		questions.push_back(question);
	}

	return questions;
}

vector<string> DataBase::getBestScores(void)
{
	vector<string> best;
	int i = 0;

	Table table = LiteQuery::Execute(DB_PATH, "select username, sum(is_correct) from t_players_answers where is_correct=1 group by username order by sum(is_correct) desc limit 3;");

	for (Row row : table.getRows())
	{
		stringstream ss;

		ss << std::setfill('0') << std::setw(2) << row["username"].length() << row["username"];
		ss << std::setfill('0') << std::setw(6) << row["sum(is_correct)"];

		best.push_back(ss.str());
		i++;
	}

	while (i++ < 3)
		best.push_back("00000000");

	return best;
}

vector<string> DataBase::getPersonalStatus(const string& username)
{
	vector<string> status;
	stringstream ss;

	int gamesNo = 0;
	int corrAns = 0;
	int inCorrAns = 0;
	double avgTime = 0;

	double totalTime = 0;
	int count = 0;

	ss << "select * from t_players_answers where username='" << username << "';";

	Table table = LiteQuery::Execute(DB_PATH, ss.str());

	for (Row row : table.getRows())
	{
		if (row["is_correct"] == "1")
			corrAns++;
		else
			inCorrAns++;

		totalTime += atof(row["answer_time"].c_str());
		count++;
	}

	avgTime = totalTime / count;
	avgTime *= 100;

	ss.str("");

	ss << " select distinct game_id from t_players_answers where username='" << username << "';";

	table = LiteQuery::Execute(DB_PATH, ss.str());
	gamesNo = table.getRows().size();

	status.push_back(std::to_string(gamesNo));
	status.push_back(std::to_string(corrAns));
	status.push_back(std::to_string(inCorrAns));
	status.push_back(std::to_string(avgTime));

	return status;
}

int DataBase::insertNewGame(void)
{
	int id = ++_currGameId;
	stringstream ss;

	ss << "insert into t_games values(" << id << "," << 0 << ",'now',null);";

	LiteQuery::ExecuteDML(DB_PATH, ss.str());

	return id;
}

bool DataBase::updateGameStatus(int gameId)
{
	stringstream ss;

	ss << "update t_games set status=1, end_time='now' where game_id=" << gameId << ";";

	try
	{
		LiteQuery::ExecuteDML(DB_PATH, ss.str());
	}
	catch (...)
	{
		return false;
	}

	return true;
}

bool DataBase::addAnswerToPlayer(int gameId, const string& username, int questionsId, const string& answer, bool isCorrect, int answerTime)
{
	stringstream ss;

	ss << "insert into t_players_answers values(";
	ss << gameId << ",'";
	ss << username << "',";
	ss << questionsId << ",'";
	ss << answer << "',";
	ss << isCorrect << ",";
	ss << answerTime << ");";

	try
	{
		LiteQuery::ExecuteDML(DB_PATH, ss.str());
	}
	catch (...)
	{
		return false;
	}

	return true;
}