#pragma once

#include <vector>
#include <map>
#include <functional>
#include <iomanip>
#include <set>

#include "Question.h"
#include "LiteQuery.h"

#define DB_PATH "trivia.db"

using std::vector;
using std::map;
using std::function;
using std::set;

class DataBase
{
public:
	DataBase();
	~DataBase();
	
	bool isUserExists(const string& username);
	bool addNewUser(const string& username, const string& password, const string& email);
	bool isUserAndPassMatch(const string& username, const string& password);
	vector<Question*> initQuestions(int questionsNo);
	vector<string> getBestScores(void);
	vector<string> getPersonalStatus(const string& username);
	int insertNewGame(void);
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int gameId, const string& username, int questionsId, const string& answer, bool isCorrect, int answerTime);

private:
	int _currGameId;
};