#include "Game.h"

#include <iomanip>

Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) : _db(db)
{
	try
	{
		_id = _db.insertNewGame();
	}
	catch(...)
	{
		throw exception("Can't insert game to data base.");
	}

	_questions = _db.initQuestions(questionsNo);
	_players = players;

	for (User* user : _players)
	{
		_results[user->getUsername()] = 0;
		user->setGame(this);
		user->clearRoom();
	}
}

Game::~Game()
{
	for (Question* q : _questions)
		delete q;
}

void Game::sendFirstQuestion(void)
{
	sendQuestionToAllUsers();
}

void Game::handleFinishGame(void)
{
	_db.updateGameStatus(_id);
	
	for (User* user : _players)
	{
		stringstream ss;

		ss << 121 << _players.size();

		for (User* user : _players)
		{
			ss << std::setfill('0') << std::setw(2) << user->getUsername().length();
			ss << user->getUsername();
			ss << std::setfill('0') << std::setw(2) << _results[user->getUsername()];
		}

		try
		{
			user->send(ss.str());
			user->setGame(nullptr);
		}
		catch (...) {}
	}
}

bool Game::handleNextTurn(void)
{
	if (!_players.size())
		handleFinishGame();
	else
	{
		if (_currentTurnAnswers == _players.size())
		{
			if (_currQuestionIndex == _questions.size() - 1)
				handleFinishGame();
			else
			{
				_currQuestionIndex++;
				sendQuestionToAllUsers();
			}
		}

		return true;
	}

	return false;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	bool correct = false;
	string answer;

	if (answerNo == 5)
		answer = ""; 
	else
		answer = _questions[_currQuestionIndex]->getAnswers()[answerNo - 1];;

	_currentTurnAnswers++;

	if (answerNo - 1 == _questions[_currQuestionIndex]->getCorrectAnswerIndex())
	{
		_results[user->getUsername()]++;
		correct = true;
	}

	_db.addAnswerToPlayer(_id, user->getUsername(), _questions[_currQuestionIndex]->getId(), answer, correct, time);

	user->send(correct ? "1201" : "1200");

	return handleNextTurn();
}

bool Game::leaveGame(User* user)
{
	auto it = std::find(_players.begin(), _players.end(), user);
	if (it != _players.end())
		_players.erase(it);

	return handleNextTurn();
}

int Game::getID(void) const
{
	return _id;
}

void Game::sendChatMessage(const string& msg, User* excludeUser)
{
	for (User* user : _players)
		if (user != excludeUser)
			user->send(msg);
}

void Game::sendQuestionToAllUsers(void)
{
	Question* question = _questions[_currQuestionIndex];
	_currentTurnAnswers = 0;

	for (User* user : _players)
	{
		stringstream ss;

		ss << 118;
		ss << std::setfill('0') << std::setw(3) << question->getQuestion().length();
		ss << question->getQuestion();

		for (int i = 0; i < 4; i++)
		{
			ss << std::setfill('0') << std::setw(3) << question->getAnswers()[i].length();
			ss << question->getAnswers()[i];
		}

		try
		{
			user->send(ss.str());
		}
		catch(...) {}
	}
}