#pragma once

#include <map>

#include "Question.h"
#include "User.h"
#include "DataBase.h"

using std::map;

class Game
{
public:
	Game(const vector<User*>& players, int questionsNo, DataBase& db);
	~Game();

	void sendFirstQuestion(void);
	void handleFinishGame(void);
	bool handleNextTurn(void);
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* user);
	int getID(void) const;

	void sendChatMessage(const string& msg, User* excludeUser);

private:
	void sendQuestionToAllUsers(void);

private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	int _currentTurnAnswers;

	int _id;
};