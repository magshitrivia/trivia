#include "LiteQuery.h"

int callback(void* notUsed, int argc, char** argv, char** azCol);

void LiteQuery::ExecuteDML(const string& dbPath, const string& query)
{
	int rc;
	sqlite3* db;
	char* zErrMsg = 0;

	rc = sqlite3_open(dbPath.c_str(), &db);

	if (rc)
	{
		const char* err = sqlite3_errmsg(db);
		sqlite3_close(db);
		throw exception(err);
	}

	rc = sqlite3_exec(db, query.c_str(), NULL, NULL, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		string err(zErrMsg);
		sqlite3_free(zErrMsg);
		throw exception(err.c_str());
	}

	sqlite3_close(db);
}

Table LiteQuery::Execute(const string& dbPath, const string& query)
{
	Table table;

	int rc;
	sqlite3* db;
	char* zErrMsg = 0;

	rc = sqlite3_open(dbPath.c_str(), &db);

	if (rc)
	{
		const char* err = sqlite3_errmsg(db);
		sqlite3_close(db);
		throw exception(err);
	}

	rc = sqlite3_exec(db, query.c_str(), callback, &table, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		string err(zErrMsg);
		sqlite3_free(zErrMsg);
		throw exception(err.c_str());
	}

	sqlite3_close(db);

	return table;
}

void LiteQuery::Transaction(const string& dbPath, vector<string> queries)
{
	ExecuteDML(dbPath, "begin transaction;");

	for(string query : queries)
		ExecuteDML(dbPath, query);

	ExecuteDML(dbPath, "commit;");
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	Table* table = (Table*)notUsed;

	if (table->getColumns().size() == 0)
		for (int i = 0; i < argc; i++)
			table->addColumn(string(azCol[i]));

	Row row;

	for (int i = 0; i < argc; i++)
	{
		row.addElement(string(azCol[i]), string(argv[i]));
	}

	table->addRow(row);

	return 0;
}