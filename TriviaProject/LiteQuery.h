#pragma once

#include <string>
#include <exception>
#include <vector>
#include "Table.h"
#include "sqlite3.h"
#include "Row.h"

using std::string;
using std::exception;
using std::vector;

class LiteQuery
{
public:
	static void ExecuteDML(const string& dbPath, const string& query);
	static Table Execute(const string& dbPath, const string& query);
	static void Transaction(const string& dbPath, vector<string> queries);
};