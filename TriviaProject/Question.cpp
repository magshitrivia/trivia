#include "Question.h"

#include <random>
#include <time.h>

using std::rand;

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	_id = id;
	_question = question;

	srand(time(NULL));

	bool finish = false;
	string tmp[] = { correctAnswer, answer2, answer3, answer4 };

	for (string ans : tmp)
	{
		do
		{
			int i = rand() % 4;

			if (_answers[i] == "")
			{
				if (ans == correctAnswer)
					_correctAnswerIndex = i;

				_answers[i] = ans;
				finish = true;
			}

		} while (!finish);

		finish = false;
	}
}

string Question::getQuestion(void) const
{
	return _question;
}

string* Question::getAnswers(void)
{
	return _answers;
}

int Question::getCorrectAnswerIndex(void) const
{
	return _correctAnswerIndex;
}

int Question::getId(void) const
{
	return _id;
}