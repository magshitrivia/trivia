#pragma once

#include <string>

using std::string;

class Question
{
public:
	Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4);

	string getQuestion(void) const;
	string* getAnswers(void);
	int getCorrectAnswerIndex(void) const;	
	int getId(void) const;
private:
	string _question;
	string _answers[4];
	int _correctAnswerIndex;
	int _id;
};