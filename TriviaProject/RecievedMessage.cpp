#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(Socket* sock, int code)
{
	_sock = sock;
	_messageCode = code;
	_user = NULL;
}

RecievedMessage::RecievedMessage(Socket* sock, int code, vector<string> values)
{
	_sock = sock;
	_messageCode = code;
	_values = values;
	_user = NULL;
}

RecievedMessage::~RecievedMessage()
{

}

Socket* RecievedMessage::getSock(void) const
{
	return _sock;
}

User* RecievedMessage::getUser(void) const
{
	return _user;
}

void RecievedMessage::setUser(User* user)
{
	_user = user;
}

int RecievedMessage::getMessageCode(void) const
{
	return _messageCode;
}

vector<string>& RecievedMessage::getValues(void) 
{
	return _values;
}