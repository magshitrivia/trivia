#pragma once

#include <vector>

#include "User.h"

using std::vector;

class RecievedMessage
{
public:
	RecievedMessage(Socket* sock, int code);
	RecievedMessage(Socket* sock, int code, vector<string> values);

	~RecievedMessage();

	Socket* getSock(void) const;
	User* getUser(void) const;
	void setUser(User* user);
	int getMessageCode(void) const;
	vector<string>& getValues(void);

private:
	Socket* _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
};