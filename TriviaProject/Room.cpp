#include "Room.h"
#include "User.h"
#include <sstream>
#include <iomanip>

using std::stringstream;

Room::Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime) : _users()
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionsNo = questionsNo;
	_questionTime = questionTime;

	_users.push_back(_admin);
}

Room::~Room()
{
	_users.clear();
}

bool Room::joinRoom(User* user)
{
	if (_users.size() == _maxUsers)
	{
		// The error msg.
		user->send("1101");
		return false;
	}

	_users.push_back(user);

	stringstream ss;

	ss << "1100";
	ss << std::setfill('0') << std::setw(2) << _questionsNo;
	ss << std::setfill('0') << std::setw(2) << _questionTime;

	user->send(ss.str());

	// Success msg.
	sendMessage(getUsersListMessage());

	return true;
}

void Room::leaveRoom(User* user)
{
	auto it = std::find(_users.begin(), _users.end(), user);
	if (it != _users.end())
	{
		_users.erase(it);

		user->clearRoom();

		// Success leaving.
		user->send("1120");

		sendMessage(user, getUsersListMessage());
	}
}

int Room::closeRoom(User* user)
{
	if (user == _admin)
	{
		for (User* user : _users)
		{
			if (user != _admin)
				user->clearRoom();

			user->send("116");
		}

		return _id;
	}

	return -1;
}

vector<User*> Room::getUsers(void) const
{
	return _users;
}

string Room::getUsersListMessage(void) const
{
	stringstream ss;

	ss << "108" << _users.size();

	for (User* user : _users)
		ss << std::setfill('0') << std::setw(2) << user->getUsername().length() << user->getUsername();

	return ss.str();
}

int Room::getQuestionsNo(void) const
{
	return _questionsNo;
}

int Room::getId(void) const
{
	return _id;
}

string Room::getName(void) const
{
	return _name;
}

void Room::sendChatMessage(const string& msg, User* excludeUser)
{
	sendMessage(excludeUser, msg);
}

string Room::getUsersAsString(vector<User*> usersList, User* excludeUser)
{
	return string();
}

void Room::sendMessage(const string& msg)
{
	sendMessage(nullptr, msg);
}

void Room::sendMessage(User* excludeUser, const string& msg)
{
	for (User* user : _users)
	{
		if (user != excludeUser)
		{
			try
			{
				user->send(msg);
			}
			catch (...)
			{
				// Some error.
			}
		}
	}
}