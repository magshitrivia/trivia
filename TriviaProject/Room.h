#pragma once

#include <vector>
#include <string>

using std::vector;
using std::string;

class User;

class Room
{
public:
	Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime);
	~Room();

	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	vector<User*> getUsers(void) const;
	string getUsersListMessage(void) const;
	int getQuestionsNo(void) const;
	int getId(void) const;
	string getName(void) const;

	void sendChatMessage(const string& msg, User* excludeUser);

private:
	string getUsersAsString(vector<User*> usersList, User* excludeUser);
	void sendMessage(const string& msg);
	void sendMessage(User* excludeUser, const string& msg);

private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionsNo;
	string _name;
	int _id;
};