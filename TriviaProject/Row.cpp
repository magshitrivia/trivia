#include "Row.h"

Row::Row() {}

Row::~Row() {}

void Row::addElement(string& column, string& value)
{
	elements.push_back(std::make_pair(column, value));
}

string& Row::operator[](const string& column)
{
	for (unsigned int i = 0; i < elements.size(); i++)
	{
		if (elements[i].first == column)
		{
			return elements[i].second;
		}
	}

	return string();
}

string Row::toString(void)
{
	stringstream s;

	for (auto element : elements)
		s << '|' << element.second << '\t';

	return s.str();
}