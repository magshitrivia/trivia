#include "Socket.h"

Socket::Socket()
{
	WSADATA wsaData;

	listenSocket = INVALID_SOCKET;
	clientSocket = INVALID_SOCKET;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0)
		throw exception("WSAStartup failed.");
}

Socket::~Socket()
{
	if(listenSocket)
		closesocket(listenSocket);
	if (clientSocket)
		closesocket(clientSocket);

	WSACleanup();
}

Socket* Socket::Accept(void)
{
	Socket* socket = new Socket;

	SOCKET newClient = accept(listenSocket, NULL, NULL);
	if (newClient == INVALID_SOCKET)
	{
		closesocket(newClient);
		//WSACleanup();
		delete socket;
		throw exception("accept failed.");
	}

	socket->clientSocket = newClient;

	return socket;
}

void Socket::Bind(const SocketAddress& addr)
{
	listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(listenSocket == INVALID_SOCKET)
		throw exception("Can't create socket.");
	
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(addr.getPort());
	sa.sin_family = AF_INET;  
	sa.sin_addr.s_addr = INADDR_ANY;

	if (bind(listenSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
}

void Socket::Close(void)
{
	if(listenSocket)
		closesocket(listenSocket);
	if(clientSocket)
		closesocket(clientSocket);
}

void Socket::Connect(const SocketAddress& addr)
{
	clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (clientSocket == INVALID_SOCKET)
		throw exception("Can't create socket.");

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(addr.getPort()); // port that server will listen to
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = inet_addr(addr.getIP().c_str());    // the IP of the server

	if (connect(clientSocket, (struct sockaddr*)&sa, sizeof(sa)) == INVALID_SOCKET)
		throw exception("Cant connect to server");
}

void Socket::Listen(unsigned int count)
{
	// Start listening for incoming requests of clients
	if (listen(listenSocket, count) == SOCKET_ERROR)
		throw std::exception("listen failed.");
}

string Socket::Receive(int len)
{
	char* buff = new char[len];

	if (clientSocket != INVALID_SOCKET)
	{
		iResult = recv(clientSocket, buff, len, 0);
		if (iResult > 0)
		{
			string res = string(buff, len);
			delete[] buff;
			return res;
		}
		else if (iResult == 0)
			throw exception("Connection closing...");
		else
			throw exception("recv failed.");
	}

	return string();
}

void Socket::Send(const string& data)
{
	if (send(clientSocket, data.c_str(), data.length(), 0) == SOCKET_ERROR)
	{
		closesocket(clientSocket);
		//WSACleanup();
		throw exception("send faild.");
	}
}