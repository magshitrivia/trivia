#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <string>
#include <exception>
#include "SocketAddress.h"

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

using std::string;
using std::exception;

class Socket
{
public:
	Socket();
	~Socket();

	Socket* Accept(void);
	void Bind(const SocketAddress& addr);
	void Close(void);
	void Connect(const SocketAddress& addr);
	void Listen(unsigned int count);
	string Receive(int len);
	void Send(const string& data);

private:
	SOCKET listenSocket; // Used if the socket is the server.
	SOCKET clientSocket; // Used if the socket is the client.
	int iResult;
};