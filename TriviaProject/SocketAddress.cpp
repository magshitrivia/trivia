#include "SocketAddress.h"

SocketAddress::SocketAddress(const string& ip, const unsigned port)
{
	this->ip = ip;
	this->port = port;
}

SocketAddress::~SocketAddress() {}

string SocketAddress::getIP(void) const
{
	return ip;
}

unsigned int SocketAddress::getPort(void) const
{
	return port;
}