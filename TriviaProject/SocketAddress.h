#pragma once

#include <string>

using std::string;

class SocketAddress
{
public:
	SocketAddress(const string& ip, const unsigned port);
	~SocketAddress();

	string getIP(void) const;
	unsigned int getPort(void) const;

private:
	string ip;
	unsigned int port;
};