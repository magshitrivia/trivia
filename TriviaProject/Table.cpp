#include "Table.h"

Table::Table() {}

Table::~Table() {}

void Table::addColumn(const string& column)
{
	if(std::find(columns.begin(), columns.end(), column) == columns.end())
		columns.push_back(column);
}

void Table::addRow(Row& row)
{
	rows.push_back(row);
}

vector<string> Table::getColumns(void) const
{
	return columns;
}

vector<Row> Table::getRows(void) const
{
	return rows;
}

Row& Table::operator[](int i)
{
	return rows[i];
}

string Table::toString(void)
{
	stringstream s;

	for (string column : columns)
		s << '|' << column << "\t";

	s << endl;

	int len = (columns.size() - 1) * 7 + columns.size() + columns.back().size();

	for (int i = 0; i < len; i++)
		s << "_";

	s << endl;

	for (Row row : rows)
		s << row.toString() << endl;

	return s.str();
}