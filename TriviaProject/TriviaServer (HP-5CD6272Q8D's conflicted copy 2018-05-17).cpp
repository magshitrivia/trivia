#include "TriviaServer.h"

int TriviaServer::_roomIdSequence = 0;

TriviaServer::TriviaServer() : _socket()
{
	// create a DataBase object.
	// create a socket object. throw exception if faild.

}

TriviaServer::~TriviaServer()
{
	_socket.Close();

	for (auto user : _connectedUsers)
	{
		delete user.first;
		delete user.second;
	}

	for (auto room : _roomsList)
		delete room.second;

	_connectedUsers.clear();
	_roomsList.clear();
}

void TriviaServer::server(void)
{
	cout << "Starting..." << endl;

	bindAndListen();

	thread t(&TriviaServer::handleRecievedMessages, this);
	t.detach();

	while (true)
		accept();
}

void TriviaServer::bindAndListen(void)
{
	_socket.Bind(SocketAddress(IP, PORT));
	cout << "binded" << endl;

	_socket.Listen(MAX_CONNECTIONS);
	cout << "listening (Port = 8820)..." << endl;
}

void TriviaServer::accept(void)
{
	cout << "accepting client..." << endl;

	Socket* client = _socket.Accept();

	thread t(&TriviaServer::clientHandler, this, client);
	t.detach();
}

void TriviaServer::clientHandler(Socket* client)
{
	bool exit = false;
	string msg;

	while (!exit)
	{
		try 
		{
			msg = client->Receive(3);

			int code = atoi(msg.c_str());

			if (code == REQ_EXIT_APPLICATION || code == 0)
				exit = true;

			RecievedMessage* msg = buildRecievedMessages(client, code);
			addRecievedMessages(msg);
		}
		catch (...)
		{
			exit = true;
		}
	}
}

void TriviaServer::safeDeleteUser(RecievedMessage* msg)
{

}

User* TriviaServer::handleSignin(RecievedMessage* msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];

	if (!_db.isUserAndPassMatch(username, password))
		msg->getSock()->Send("1021");
	else 
	{
		if (_connectedUsers.find(msg->getSock()) != _connectedUsers.end())
			msg->getSock()->Send("1022");
		else
		{
			User* user = new User(username, msg->getSock());
			_connectedUsers[msg->getSock()] = user;
			user->getSocket()->Send("1020");
			return user;
		}
	}

	return nullptr;
}

bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];
	string email = msg->getValues()[2];

	if (!Validator::isPasswordVaild(password))
	{
		msg->getSock()->Send("1041");
		return false;
	}

	if (!Validator::isUsernameVaild(username))
	{
		msg->getSock()->Send("1043");
		return false;
	}

	if (_db.isUserExists(username))
	{
		msg->getSock()->Send("1042");
		return false;
	}

	if (!_db.addNewUser(username, password, email))
	{
		msg->getSock()->Send("1044");
		return false;
	}

	msg->getSock()->Send("1040");
	return true;

}

void TriviaServer::handleSignout(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		Socket* sock = msg->getSock();
		User* user = msg->getUser();

		_connectedUsers.erase(sock);

		delete sock;
		delete user;

		handleCloseRoom(msg);
		handleLeaveRoom(msg);
		handleLeaveGame(msg);
	}
}

void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{

}

void TriviaServer::handleStartGame(RecievedMessage* msg)
{

}

void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{

}

bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		User* user = msg->getUser();

		string roomName = msg->getValues()[0];
		int playersCount = atoi(msg->getValues()[1].c_str());
		int questionCount = atoi(msg->getValues()[2].c_str());
		int questionTime = atoi(msg->getValues()[3].c_str());

		_roomIdSequence++;

		if (user->createRoom(_roomIdSequence, roomName, playersCount, questionCount, questionTime))
		{
			_roomsList[_roomIdSequence] = new Room(_roomIdSequence, user, roomName, playersCount, questionCount, questionTime);
			return true;
		}
	}

	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		int roomId = msg->getUser()->getRoom()->getId();

		if (msg->getUser()->closeRoom() != -1)
		{
			Room* room = _roomsList[roomId];
			_roomsList.erase(roomId);
			delete room;

			return true;
		}
	}

	return false;
}

bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		int roomId = atoi(msg->getValues()[0].c_str());
		
		Room* room = getRoomById(roomId);

		if (!room)
		{
			msg->getSock()->Send("1102");
			return false;
		}

		return msg->getUser()->joinRoom(room);
	}

	return false;
}

bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	return true;
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{

}

void TriviaServer::handleGetRooms(RecievedMessage* msg)
{

}

void TriviaServer::handleGetBestScores(RecievedMessage* msg)
{

}

void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg)
{

}

void TriviaServer::handleRecievedMessages(void)
{
	unique_lock<mutex> lock(_mtxRecievedMessages);
	_cvRecievedMessages.wait(lock, [this] { return !_queRcvMessages.empty(); });

	RecievedMessage* msg = _queRcvMessages.front();

	lock.unlock();

	msg->setUser(getUserBySocket(msg->getSock()));

	if (msg->getMessageCode() == REQ_SIGN_IN)
		msg->setUser(handleSignin(msg));
	else if (msg->getMessageCode() == REQ_SIGN_OUT)
		handleSignout(msg);
	else if (msg->getMessageCode() == REQ_SIGN_UP)
		handleSignup(msg);
	else if (msg->getMessageCode() == REQ_CREATE_ROOM)
		handleCreateRoom(msg);
	else if (msg->getMessageCode() == REQ_CLOSE_ROOM)
		handleCloseRoom(msg);
	else if (msg->getMessageCode() == REQ_JOIN_ROOM)
		handleJoinRoom(msg);
}

void TriviaServer::addRecievedMessages(RecievedMessage* msg)
{
	{
		lock_guard<mutex> lock(_mtxRecievedMessages);
		_queRcvMessages.push(msg);
	}

	// Wake CV.
	_cvRecievedMessages.notify_one();
}

RecievedMessage* TriviaServer::buildRecievedMessages(Socket* client, int msgCode)
{
	if (msgCode == REQ_SIGN_IN)
	{
		int usernameLen = atoi(client->Receive(2).c_str());
		string username = client->Receive(usernameLen);

		int passwordLen = atoi(client->Receive(2).c_str());
		string password = client->Receive(passwordLen);

		vector<string> data;

		data.push_back(username);
		data.push_back(password);

		return new RecievedMessage(client, msgCode, data);
	}
	else if (msgCode == REQ_SIGN_OUT || msgCode == REQ_EXIST_ROOMS || msgCode == REQ_LEAVE_ROOM || msgCode == REQ_CLOSE_ROOM || msgCode == REQ_EXIT_APPLICATION)
	{
		return new RecievedMessage(client, msgCode);
	}
	else if (msgCode == REQ_SIGN_UP)
	{
		int usernameLen = atoi(client->Receive(2).c_str());
		string username = client->Receive(usernameLen);

		int passwordLen = atoi(client->Receive(2).c_str());
		string password = client->Receive(passwordLen);

		int emailLen = atoi(client->Receive(2).c_str());
		string email = client->Receive(emailLen);

		vector<string> data;

		data.push_back(username);
		data.push_back(password);
		data.push_back(email);

		return new RecievedMessage(client, msgCode, data);
	}
	else if (msgCode == REQ_USERS_OF_ROOM || msgCode == REQ_JOIN_ROOM)
	{
		string roomId = client->Receive(4);

		vector<string> data;

		data.push_back(roomId);
		
		return new RecievedMessage(client, msgCode, data);
	}
	else if (msgCode == REQ_CREATE_ROOM)
	{
		int roomNameLen = atoi(client->Receive(2).c_str());
		string roomName = client->Receive(roomNameLen);

		string playersCount = client->Receive(1).c_str();
		string questionCount = client->Receive(2).c_str();
		string questionTime = client->Receive(2).c_str();

		vector<string> data;

		data.push_back(roomName);
		data.push_back(playersCount);
		data.push_back(questionCount);
		data.push_back(questionTime);

		return new RecievedMessage(client, msgCode, data);
	}

	return nullptr;
}

User* TriviaServer::getUserByName(const string& name)
{
	for (auto user : _connectedUsers)
		if (user.second->getUsername() == name)
			return user.second;

	return nullptr;
}

User* TriviaServer::getUserBySocket(Socket* client)
{
	if(_connectedUsers.find(client) == _connectedUsers.end())
		return nullptr;

	return _connectedUsers[client];
}

Room* TriviaServer::getRoomById(int id)
{
	if (_roomsList.find(id) == _roomsList.end())
		return nullptr;

	return _roomsList[id];
}