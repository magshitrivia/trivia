#include "TriviaServer.h"

// Function for debug purpose.
void printMsg(RecievedMessage* msg)
{
	if(msg->getUser())
		cout << "Username: " << msg->getUser()->getUsername() << endl;

	cout << "Code: " << msg->getMessageCode() << endl;
	cout << "Values:" << endl;

	for (string val : msg->getValues())
		cout << val << endl;
}

int TriviaServer::_roomIdSequence = 0;

TriviaServer::TriviaServer() : _socket() {}

TriviaServer::~TriviaServer()
{
	_socket.Close();

	for (auto user : _connectedUsers)
	{
		delete user.first;
		delete user.second;
	}

	for (auto room : _roomsList)
		delete room.second;

	_connectedUsers.clear();
	_roomsList.clear();
}

void TriviaServer::server(void)
{
	cout << "Starting..." << endl;

	bindAndListen();

	thread t(&TriviaServer::handleRecievedMessages, this);
	t.detach();

	while (true)
		accept();
}

void TriviaServer::bindAndListen(void)
{
	_socket.Bind(SocketAddress(IP, PORT));
	cout << "binded." << endl;

	_socket.Listen(MAX_CONNECTIONS);
	cout << "listening (Port = 8820)..." << endl;
}

void TriviaServer::accept(void)
{
	cout << "accepting client..." << endl;

	Socket* client = _socket.Accept();

	thread t(&TriviaServer::clientHandler, this, client);
	t.detach();
}

void TriviaServer::clientHandler(Socket* client)
{
	bool exit = false;

	// Exchange keys between client and server.
	device.setOtherPublicKey(client->Receive(1024));
	client->Send(device.getPublicKey());

	while (!exit)
	{
		try
		{
			// Check if the message encrypted.
			int isEncrypted = atoi(client->Receive(1).c_str());
			RecievedMessage* msg = nullptr;
			
			if (isEncrypted == 1)
			{
				string cipher = client->Receive(1024);
				string plain = device.decrypt(cipher);

				int code = atoi(plain.substr(0, 3).c_str());
				vector<string> data;

				if (code == REQ_SIGN_IN)
				{
					int usernameLen = atoi(plain.substr(3, 2).c_str());
					string username = plain.substr(5, usernameLen);

					int passwordLen = atoi(plain.substr(5 + usernameLen, 2).c_str());
					string password = plain.substr(usernameLen + 7, passwordLen);

					data.push_back(username);
					data.push_back(password);
				}
				else if (code == REQ_SIGN_UP)
				{
					int usernameLen = atoi(plain.substr(3, 2).c_str());
					string username = plain.substr(5, usernameLen);

					int passwordLen = atoi(plain.substr(5 + usernameLen, 2).c_str());
					string password = plain.substr(usernameLen + 7, passwordLen);

					int emailLen = atoi(plain.substr(7 + usernameLen + passwordLen, 2).c_str());
					string email = plain.substr(9 + usernameLen + passwordLen, emailLen);

					data.push_back(username);
					data.push_back(password);
					data.push_back(email);

				}

				msg = new RecievedMessage(client, code, data);
			}
			else if(isEncrypted == 0)
			{
				int code = atoi(client->Receive(3).c_str());

				if (code == REQ_EXIT_APPLICATION || code == 0)
					exit = true;

				msg = buildRecievedMessages(client, code);
			}

			addRecievedMessages(msg);
		}
		catch (...)
		{
			exit = true;
		}
	}
}

void TriviaServer::safeDeleteUser(RecievedMessage* msg)
{
	handleSignout(msg);

	if (msg->getSock())
		msg->getSock()->Close();

	delete msg->getSock();
}

User* TriviaServer::handleSignin(RecievedMessage* msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];

	if (!_db.isUserAndPassMatch(username, password))
		msg->getSock()->Send("1021");
	else
	{
		if (getUserByName(username))
			msg->getSock()->Send("1022");
		else
		{
			User* user = new User(username, msg->getSock());
			_connectedUsers[msg->getSock()] = user;
			user->getSocket()->Send("1020");
			return user;
		}
	}

	return nullptr;
}

bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];
	string email = msg->getValues()[2];

	if (!Validator::isPasswordVaild(password))
	{
		msg->getSock()->Send("1041");
		return false;
	}

	if (!Validator::isUsernameVaild(username))
	{
		msg->getSock()->Send("1043");
		return false;
	}

	if (_db.isUserExists(username))
	{
		msg->getSock()->Send("1042");
		return false;
	}

	if (!_db.addNewUser(username, password, email))
	{
		msg->getSock()->Send("1044");
		return false;
	}

	msg->getSock()->Send("1040");
	return true;

}

void TriviaServer::handleSignout(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		Socket* sock = msg->getSock();
		User* user = msg->getUser();

		_connectedUsers.erase(sock);

		handleCloseRoom(msg);
		handleLeaveRoom(msg);
		handleLeaveGame(msg);

		delete user;
	}
}

void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	if (msg->getUser() && !msg->getUser()->leaveGame())
		delete msg->getUser()->getGame();
}

void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	if (msg->getUser() && msg->getUser()->getRoom())
	{
		Room* room = msg->getUser()->getRoom();

		try
		{
			Room* r = _roomsList[room->getId()];
			_roomsList.erase(room->getId());

			Game* game = new Game(r->getUsers(), r->getQuestionsNo(), _db);

			delete r;
			delete room;

			game->sendFirstQuestion();
		}
		catch (...)
		{
			msg->getUser()->send("1180");
		}
	}
}

void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		Game* game = msg->getUser()->getGame();

		int ansNo = atoi(msg->getValues()[0].c_str());
		int time = atoi(msg->getValues()[1].c_str());

		if (game && !game->handleAnswerFromUser(msg->getUser(), ansNo, time))
			delete game;
	}
}

bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		User* user = msg->getUser();

		string roomName = msg->getValues()[0];
		int playersCount = atoi(msg->getValues()[1].c_str());
		int questionCount = atoi(msg->getValues()[2].c_str());
		int questionTime = atoi(msg->getValues()[3].c_str());

		_roomIdSequence++;

		if (user->createRoom(_roomIdSequence, roomName, playersCount, questionCount, questionTime))
		{
			_roomsList[_roomIdSequence] = new Room(_roomIdSequence, user, roomName, playersCount, questionCount, questionTime);
			return true;
		}
	}

	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	if (msg->getUser() && msg->getUser()->getRoom())
	{
		int roomId = msg->getUser()->getRoom()->getId();

		if (msg->getUser()->closeRoom() != -1)
		{
			Room* room = _roomsList[roomId];
			_roomsList.erase(roomId);
			delete room;

			return true;
		}
	}

	return false;
}

bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		int roomId = atoi(msg->getValues()[0].c_str());

		Room* room = getRoomById(roomId);

		if (!room)
		{
			msg->getSock()->Send("1102");
			return false;
		}

		return msg->getUser()->joinRoom(room);
	}

	return false;
}

bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		if (msg->getUser()->getRoom())
		{
			msg->getUser()->leaveRoom();
			return true;
		}
	}

	return false;
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	User* user = msg->getUser();
	int roomId = atoi(msg->getValues()[0].c_str());
	Room* room = getRoomById(roomId);

	user->send(!room ? "1080" : room->getUsersListMessage());
}

void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	stringstream ss;

	ss << 106;

	if (_roomsList.size())
		ss << std::setfill('0') << std::setw(4) << _roomsList.size();
	else
		ss << 0;

	for (auto room : _roomsList)
		ss << std::setfill('0') << std::setw(4) << room.first << std::setfill('0') << std::setw(2) << room.second->getName().length() << room.second->getName();

	msg->getUser()->send(ss.str());
}

void TriviaServer::handleGetBestScores(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		vector<string> scores = _db.getBestScores();
		stringstream ss;

		ss << 124;

		for (string score : scores)
			ss << score;

		msg->getUser()->send(ss.str());
	}
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg)
{
	if (msg->getUser())
	{
		vector<string> status = _db.getPersonalStatus(msg->getUser()->getUsername());
		stringstream ss;
		ss << 126;

		if (status.size())
		{
			ss << std::setfill('0') << std::setw(4) << atoi(status[0].c_str());
			ss << std::setfill('0') << std::setw(6) << atoi(status[1].c_str());
			ss << std::setfill('0') << std::setw(6) << atoi(status[2].c_str());
			ss << std::setfill('0') << std::setw(4) << atoi(status[3].c_str());
		}
		else
			ss << 0000;

		msg->getUser()->send(ss.str());
	}
}

void TriviaServer::handleEmailExist(RecievedMessage* msg)
{
	string email = msg->getValues()[0];

	stringstream ss;

	ss << "select * from t_users where email='" << email << "';";

	Table table = LiteQuery::Execute(DB_PATH, ss.str());

	msg->getSock()->Send(table.getRows().size() > 0 ? "130" : "131");
}

void TriviaServer::handleResetPassword(RecievedMessage* msg)
{
	string password = msg->getValues()[0];
	string email = msg->getValues()[1];

	try
	{
		stringstream ss;

		ss << "update t_users set password='" << password << "' where email='" << email << "';";

		LiteQuery::ExecuteDML(DB_PATH, ss.str());

		msg->getSock()->Send("140");
	}
	catch (...)
	{
		// Some error.
	}
}

void TriviaServer::handleChatMessage(RecievedMessage* msg)
{
	User* user = msg->getUser();

	if (user)
	{
		string message = msg->getValues()[0];
		stringstream ss;

		ss << 150;
		ss << std::setfill('0') << std::setw(2) << user->getUsername().length() << user->getUsername();
		ss << std::setfill('0') << std::setw(2) << message.length() << message;

		if (user->getRoom())
			_roomsList[user->getRoom()->getId()]->sendChatMessage(ss.str(), user);
		else if (user->getGame())
			user->getGame()->sendChatMessage(ss.str(), user);
	}
}

void TriviaServer::handleRecievedMessages(void)
{
	while (true)
	{
		unique_lock<mutex> lock(_mtxRecievedMessages);
		_cvRecievedMessages.wait(lock, [this] { return !_queRcvMessages.empty(); });

		RecievedMessage* msg = _queRcvMessages.front();
		_queRcvMessages.pop();

		lock.unlock();

		msg->setUser(getUserBySocket(msg->getSock()));

		printMsg(msg);

		try
		{
			if (msg->getMessageCode() == REQ_SIGN_IN)
				msg->setUser(handleSignin(msg));
			else if (msg->getMessageCode() == REQ_SIGN_OUT)
				handleSignout(msg);
			else if (msg->getMessageCode() == REQ_SIGN_UP)
				handleSignup(msg);
			else if (msg->getMessageCode() == REQ_CREATE_ROOM)
				handleCreateRoom(msg);
			else if (msg->getMessageCode() == REQ_CLOSE_ROOM)
				handleCloseRoom(msg);
			else if (msg->getMessageCode() == REQ_JOIN_ROOM)
				handleJoinRoom(msg);
			else if (msg->getMessageCode() == REQ_LEAVE_ROOM)
				handleLeaveRoom(msg);
			else if (msg->getMessageCode() == REQ_EXIST_ROOMS)
				handleGetRooms(msg);
			else if (msg->getMessageCode() == REQ_USERS_OF_ROOM)
				handleGetUsersInRoom(msg);
			else if (msg->getMessageCode() == REQ_START_GAME)
				handleStartGame(msg);
			else if (msg->getMessageCode() == REQ_CLIENT_ANSWER)
				handlePlayerAnswer(msg);
			else if (msg->getMessageCode() == REQ_LEAVE_GAME)
				handleLeaveGame(msg);
			else if (msg->getMessageCode() == REQ_PERSONAL_STATUS)
				handleGetPersonalStatus(msg);
			else if (msg->getMessageCode() == REQ_BEST_SCORES)
				handleGetBestScores(msg);
			else if (msg->getMessageCode() == REQ_EMAIL_EXIST)
				handleEmailExist(msg);
			else if (msg->getMessageCode() == REQ_RESET_PASSWORD)
				handleResetPassword(msg);
			else if (msg->getMessageCode() == REQ_CHAT_MESSAGE)
				handleChatMessage(msg);
			else if (msg->getMessageCode() == REQ_EXIT_APPLICATION)
				safeDeleteUser(msg);
			else
				safeDeleteUser(msg);
		}
		catch (...)
		{
			safeDeleteUser(msg);
		}

		delete msg;
	}
}

void TriviaServer::addRecievedMessages(RecievedMessage* msg)
{
	{
		lock_guard<mutex> lock(_mtxRecievedMessages);
		_queRcvMessages.push(msg);
	}

	_cvRecievedMessages.notify_one();
}

RecievedMessage* TriviaServer::buildRecievedMessages(Socket* client, int msgCode)
{
	if (msgCode == REQ_SIGN_OUT || msgCode == REQ_EXIST_ROOMS || msgCode == REQ_LEAVE_ROOM || msgCode == REQ_CLOSE_ROOM || msgCode == REQ_EXIT_APPLICATION || msgCode == REQ_START_GAME || msgCode == REQ_LEAVE_GAME || msgCode == REQ_PERSONAL_STATUS || msgCode == REQ_BEST_SCORES)
	{
		return new RecievedMessage(client, msgCode);
	}
	else if (msgCode == REQ_USERS_OF_ROOM || msgCode == REQ_JOIN_ROOM)
	{
		string roomId = client->Receive(4);

		vector<string> data;

		data.push_back(roomId);

		return new RecievedMessage(client, msgCode, data);
	}
	else if (msgCode == REQ_CREATE_ROOM)
	{
		int roomNameLen = atoi(client->Receive(2).c_str());
		string roomName = client->Receive(roomNameLen);

		string playersCount = client->Receive(1).c_str();
		string questionCount = client->Receive(2).c_str();
		string questionTime = client->Receive(2).c_str();

		vector<string> data;

		data.push_back(roomName);
		data.push_back(playersCount);
		data.push_back(questionCount);
		data.push_back(questionTime);

		return new RecievedMessage(client, msgCode, data);
	}
	else if (msgCode == REQ_CLIENT_ANSWER)
	{
		string ansNo = client->Receive(1);
		string time = client->Receive(2);

		vector<string> data;

		data.push_back(ansNo);
		data.push_back(time);

		return new RecievedMessage(client, msgCode, data);
	}
	else if (msgCode == REQ_EMAIL_EXIST)
	{
		int emailLen = atoi(client->Receive(3).c_str());
		string email = client->Receive(emailLen);

		vector<string> data;

		data.push_back(email);

		return new RecievedMessage(client, msgCode, data);
	}
	else if (msgCode == REQ_RESET_PASSWORD)
	{
		int passwordLen = atoi(client->Receive(2).c_str());
		string password = client->Receive(passwordLen);

		int emailLen = atoi(client->Receive(3).c_str());
		string email = client->Receive(emailLen);

		vector<string> data;
		data.push_back(password);
		data.push_back(email);

		return new RecievedMessage(client, msgCode, data);
	}
	else if (msgCode == REQ_CHAT_MESSAGE)
	{
		int msgLen = atoi(client->Receive(2).c_str());
		string msg = client->Receive(msgLen);

		vector<string> data;
		data.push_back(msg);

		return new RecievedMessage(client, msgCode, data);
	}

	return nullptr;
}

User* TriviaServer::getUserByName(const string& name)
{
	for (auto user : _connectedUsers)
		if (user.second->getUsername() == name)
			return user.second;

	return nullptr;
}

User* TriviaServer::getUserBySocket(Socket* client)
{
	if (_connectedUsers.find(client) == _connectedUsers.end())
		return nullptr;

	return _connectedUsers[client];
}

Room* TriviaServer::getRoomById(int id)
{
	if (_roomsList.find(id) == _roomsList.end())
		return nullptr;

	return _roomsList[id];
}