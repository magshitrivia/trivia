#pragma once

#include <iostream>
#include <map>
#include <mutex>
#include <queue>
#include <thread>
#include <condition_variable>
#include <iomanip>

#include "User.h"
#include "DataBase.h"
#include "Room.h"
#include "RecievedMessage.h"
#include "Protocol.h"
#include "Validator.h"
#include "Game.h"
#include "CryptoDevice.h"

#define IP "127.0.0.1"
#define PORT 8820
#define MAX_CONNECTIONS 10
#define BUFFER_LEN 1024

using std::cout;
using std::endl;
using std::map;
using std::mutex;
using std::lock_guard;
using std::queue;
using std::thread;
using std::condition_variable;
using std::unique_lock;

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();

	void server(void);

private:
	void bindAndListen(void);
	void accept(void);
	void clientHandler(Socket* client);
	void safeDeleteUser(RecievedMessage* msg);

	User* handleSignin(RecievedMessage* msg);
	bool handleSignup(RecievedMessage* msg);
	void handleSignout(RecievedMessage* msg);

	void handleLeaveGame(RecievedMessage* msg);
	void handleStartGame(RecievedMessage* msg);
	void handlePlayerAnswer(RecievedMessage* msg);

	bool handleCreateRoom(RecievedMessage* msg);
	bool handleCloseRoom(RecievedMessage* msg);
	bool handleJoinRoom(RecievedMessage* msg);
	bool handleLeaveRoom(RecievedMessage* msg);
	void handleGetUsersInRoom(RecievedMessage* msg);
	void handleGetRooms(RecievedMessage* msg);

	void handleGetBestScores(RecievedMessage* msg);
	void handleGetPersonalStatus(RecievedMessage* msg);

	void handleEmailExist(RecievedMessage* msg);
	void handleResetPassword(RecievedMessage* msg);

	void handleChatMessage(RecievedMessage* msg);

	void handleRecievedMessages(void);
	void addRecievedMessages(RecievedMessage* msg);
	RecievedMessage* buildRecievedMessages(Socket* client, int msgCode);

	User* getUserByName(const string& name);
	User* getUserBySocket(Socket* client);
	Room* getRoomById(int id);

private:
	Socket _socket;
	map<Socket*, User*> _connectedUsers;
	DataBase _db;
	map<int, Room*> _roomsList;

	mutex _mtxRecievedMessages;
	queue<RecievedMessage*> _queRcvMessages;

	condition_variable _cvRecievedMessages;

	static int _roomIdSequence;

	CryptoDevice device;
};