#include "User.h"
#include "Game.h"

User::User(string username, Socket* sock)
{
	_username = username;
	_sock = sock;
	_currRoom = nullptr;
	_currGame = nullptr;
}

User::~User()
{
	if (_currRoom)
		delete _currRoom;
	if (_currGame)
		delete _currGame;
}

void User::send(string message)
{
	_sock->Send(message);
}

string User::getUsername(void) const
{
	return _username;
}

Socket* User::getSocket(void) const
{
	return _sock;
}

Room* User::getRoom(void) const
{
	return _currRoom;
}

Game* User::getGame(void) const
{
	return _currGame;
}

void User::setGame(Game* game)
{
	_currGame = game;
}

void User::clearRoom(void)
{
	_currRoom = nullptr;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (_currRoom)
	{
		_sock->Send("1141");
		return false;
	}

	_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);

	std::cout << 1140 << endl;

	_sock->Send("1140");

	return true;
}

bool User::joinRoom(Room* room)
{
	if (_currRoom)
		return false;

	if (room->joinRoom(this))
	{
		_currRoom = room;
		return true;
	}

	return false;
}

void User::leaveRoom(void)
{
	if (_currRoom)
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}

int User::closeRoom(void)
{
	if (_currRoom)
	{
		int id = _currRoom->getId();

		_currRoom->closeRoom(this);
		delete _currRoom;
		_currRoom = nullptr;

		return id;
	}

	return -1;
}

bool User::leaveGame(void)
{
	bool res = false;

	if (_currGame)
	{
		res = _currGame->leaveGame(this);
		_currGame = nullptr;
	}

	return res;
}