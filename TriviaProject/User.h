#pragma once

#include <string>
#include "Room.h"
#include "Socket.h"

class Game;

class User
{
public:
	User(string username, Socket* sock);
	~User();

	void send(string message);
	string getUsername(void) const;
	Socket* getSocket(void) const;
	Room* getRoom(void) const;
	Game* getGame(void) const;
	void setGame(Game* game);
	void clearRoom(void);
	bool createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(Room* room);
	void leaveRoom(void);
	int closeRoom(void);
	bool leaveGame(void);
private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	Socket* _sock;
};