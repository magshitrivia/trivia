#include "Validator.h"

Validator::Validator() {}

Validator::~Validator() {}

bool Validator::isPasswordVaild(const string& password)
{
	bool hasDigit = false;
	bool hasUpper = false;
	bool hasLower = false;

	if (password.length() < 4)
		return false;

	for (char c : password)
	{
		if (c == ' ')
			return false;
		else if (c >= 'a' && c <= 'z')
			hasLower = true;
		else if (c >= 'A' && c <= 'Z')
			hasUpper = true;
		else if (c >= '0' && c <= '9')
			hasDigit = true;
	}

	return hasDigit && hasUpper && hasLower;
}

bool Validator::isUsernameVaild(const string& username)
{
	if (username.length() == 0)
		return false;

	if ((username[0] < 'A' && username[0] > 'Z') || (username[0] < 'a' && username[0] > 'z'))
		return false;

	for (char c : username)
		if (c == ' ')
			return false;

	return true;
}