#pragma once

#include <string>

using std::string;

class Validator
{
public:
	static bool isPasswordVaild(const string& password);
	static bool isUsernameVaild(const string& username);
private:
	Validator();
	~Validator();
};